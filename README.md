# Firezone VPN

This module will configure the networking and instance setup for Firezone. It currently does not yet install firezone.

## Installing

```
cd terraform
ssh-keygen -t rsa -b 2048 -f ssh-keys/firezone-ssh
terraform init
terraform apply
```

## SSH'ing into the instance

```
ssh -i ssh-keys/firezone-ssh ubuntu@`terraform output -raw public_ip`
```

## Setting up Firezone

### Open the neccessary ports

```
iptables -I INPUT -p tcp -m tcp --dport 80 -j ACCEPT
iptables -I INPUT -p tcp -m tcp --dport 443 -j ACCEPT
```

### Install Docker

```
sudo apt-get update

sudo apt-get install ca-certificates curl gnupg lsb-release

sudo mkdir -m 0755 -p /etc/apt/keyrings

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo   "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

mkdir /opt/firezone

bash <(curl -fsSL https://github.com/firezone/firezone/raw/master/scripts/install.sh) 186505ebe0f991-09d71b3f4fa80e-16525635-13c680-186505ebe1016e9

sudo systemctl enable docker

```

### Backing up

See https://www.firezone.dev/docs/administer/backup/

```
sudo docker compose -f /opt/firezone/docker-compose.yml down
sudo tar zcvfp ~/firezone-back-$(date +'%F-%H-%M').tgz /opt/firezone /var/lib/docker/volumes/firezone_postgres-data
```


## Clients

https://www.firezone.dev/docs/user-guides/client-instructions/

