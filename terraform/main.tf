terraform {
  required_providers {
    oci = {
      source  = "oracle/oci"
      version = "4.105.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "4.50.0"
    }
    local = {
      source = "hashicorp/local"
    }
  }
}


