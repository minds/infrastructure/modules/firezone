#############################################
### Create a new compartment for firezone ###
#############################################

resource "oci_identity_compartment" "firezone" {
    compartment_id = var.oci_tenancy_ocid
    description = "Firezone (VPN) compartment"
    name = "minds-firezone"
}

########################
### Networking setup ###
########################

resource "oci_core_vcn" "firezone_vcn" {
  compartment_id = oci_identity_compartment.firezone.id
  cidr_block = "192.168.0.0/16"
  display_name = "minds-firezone"
}

resource "oci_core_subnet" "firezone_public" {
  cidr_block = oci_core_vcn.firezone_vcn.cidr_block
  compartment_id = oci_identity_compartment.firezone.id
  vcn_id = oci_core_vcn.firezone_vcn.id
  route_table_id = oci_core_route_table.firezone_rt.id
  display_name = "firezone-public"
}

resource "oci_core_internet_gateway" "firezone_igw" {
  compartment_id = oci_identity_compartment.firezone.id
  vcn_id = oci_core_vcn.firezone_vcn.id
  display_name = "firezone-igw"
}

resource "oci_core_route_table" "firezone_rt" {
  compartment_id = oci_identity_compartment.firezone.id
  vcn_id = oci_core_vcn.firezone_vcn.id

  route_rules {
    network_entity_id = oci_core_internet_gateway.firezone_igw.id
    destination = "0.0.0.0/0"
  }
  display_name = "firezone-main-routes"
}

### Security group ###

resource "oci_core_network_security_group" "firezone_nsg" {
  compartment_id = oci_identity_compartment.firezone.id
  vcn_id = oci_core_vcn.firezone_vcn.id
  display_name = "firezone"
}

resource "oci_core_network_security_group_security_rule" "firezone_http" {
  network_security_group_id = oci_core_network_security_group.firezone_nsg.id
  direction = "INGRESS"
  protocol = 6 # TCP

  source = "0.0.0.0/0"

  tcp_options {
    destination_port_range {
      max = 80
      min = 80
    }
  }
}

resource "oci_core_network_security_group_security_rule" "firezone_https" {
  network_security_group_id = oci_core_network_security_group.firezone_nsg.id
  direction = "INGRESS"
  protocol = 6 # TCP
  
  source = "0.0.0.0/0"

  tcp_options {
    destination_port_range {
      max = 443
      min = 443
    }
  }
}

resource "oci_core_network_security_group_security_rule" "firezone_vpn" {
  network_security_group_id = oci_core_network_security_group.firezone_nsg.id
  direction = "INGRESS"
  protocol = 17 # UDP
  
  source = "0.0.0.0/0"

  udp_options {
    destination_port_range {
      max = 51820
      min = 51820
    }
  }
}

##########################
### Setup our instance ###
##########################

resource "oci_core_instance" "firezone_docker_server" {
    availability_domain = "MMiR:US-ASHBURN-AD-1"
    compartment_id = oci_identity_compartment.firezone.id
    shape = "VM.Standard.E4.Flex"
    display_name = "firezone"

    create_vnic_details {
        subnet_id = oci_core_subnet.firezone_public.id
        assign_public_ip = false # We set this up later
        nsg_ids = [
          oci_core_network_security_group.firezone_nsg.id
        ]
    }
  
    shape_config {
        memory_in_gbs = 2
        ocpus = 1
    }
  
    source_details {
        source_id = "ocid1.image.oc1.iad.aaaaaaaa65w7hi5ph6gnddni5i6xijrpwinihowqerkasdsaslxo376ris2q"
        source_type = "image"
    }

    metadata = { 
      ssh_authorized_keys = file("./ssh-keys/firezone-ssh.pub")
    }
  
    preserve_boot_volume = false
}

data "oci_core_private_ips" "firezone_private_ip" {
  ip_address = oci_core_instance.firezone_docker_server.private_ip
  subnet_id = oci_core_subnet.firezone_public.id
}

resource "oci_core_public_ip" "firezone_stable_ip" {
  compartment_id = oci_identity_compartment.firezone.id
  lifetime = "RESERVED"
  private_ip_id = data.oci_core_private_ips.firezone_private_ip.private_ips[0].id
}


#############################################################
### Output the IP address so we can update the DNS Record ###
#############################################################

output "public_ip" {
  value = oci_core_public_ip.firezone_stable_ip.ip_address
}