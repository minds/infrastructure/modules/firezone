###########
### OCI ###
###########

variable "oci_region" {
  type        = string
  description = "Target OCI region."

  default = "us-ashburn-1"
}

variable "oci_tenancy_ocid" {
  type        = string
  description = "Tenancy ID for the target Oracle cloud account."

  default = "ocid1.tenancy.oc1..aaaaaaaa24wrydmie6u35gxqyeqnt2g4swp2kge3jomukit34tjy6kokqknq" # mindsoci
}